const express = require('express')
const cors =require('cors')
const app = express()

// middlewares 
app.use(cors())
app.use(express.json())
app.use(express.urlencoded({extended:false}))

// rutas
app.use(require('./routes/routes'))

app.listen(8080)
app.get("/", (req, res) => {
    res.json({ message: "Bienvenido al back-end" });
});