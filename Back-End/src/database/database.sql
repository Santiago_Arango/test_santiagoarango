-- Table: public.ciudades

-- DROP TABLE public.ciudades;

CREATE TABLE public.ciudades
(
    id_ciudad integer NOT NULL DEFAULT nextval('ciudades_id_ciudad_seq'::regclass),
    nombre character varying(100) COLLATE pg_catalog."default" NOT NULL,
    id_estado bigint NOT NULL,
    poblacion bigint NOT NULL,
    CONSTRAINT ciudades_pkey1 PRIMARY KEY (id_ciudad)
)

TABLESPACE pg_default;

ALTER TABLE public.ciudades
    OWNER to postgres;


-- Table: public.estados

-- DROP TABLE public.estados;

CREATE TABLE public.estados
(
    id_estado integer NOT NULL DEFAULT nextval('"Estados_idEstado_seq"'::regclass),
    nombre character varying(100) COLLATE pg_catalog."default" NOT NULL,
    id_pais bigint NOT NULL,
    CONSTRAINT "Estados_pkey" PRIMARY KEY (id_estado)
)

TABLESPACE pg_default;

ALTER TABLE public.estados
    OWNER to postgres;

-- Table: public.paises

-- DROP TABLE public.paises;

CREATE TABLE public.paises
(
    id_pais integer NOT NULL DEFAULT nextval('"Paises_IdPais_seq"'::regclass),
    nombre character varying(255) COLLATE pg_catalog."default" NOT NULL,
    CONSTRAINT "Paises_pkey" PRIMARY KEY (id_pais)
)

TABLESPACE pg_default;

ALTER TABLE public.paises
    OWNER to postgres;

-- Table: public.consolidados

-- DROP TABLE public.consolidados;

CREATE TABLE public.consolidados
(
    nombre_ciudad character varying(255) COLLATE pg_catalog."default",
    nombre_estado character varying(255) COLLATE pg_catalog."default",
    nombre_pais character varying(255) COLLATE pg_catalog."default",
    poblacion bigint,
    id integer NOT NULL DEFAULT nextval('consolidados_id_seq'::regclass)
)

TABLESPACE pg_default;

ALTER TABLE public.consolidados
    OWNER to postgres;