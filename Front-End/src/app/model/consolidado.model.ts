export class Consolidado {
    constructor(
        public id? : number,
        public nombre_ciudad? : string,
        public nombre_estado? : string,
        public nombre_pais? : string,
        public poblacion? : number
    ){

    }
}