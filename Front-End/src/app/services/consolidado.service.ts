import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http'
import {Consolidado} from '../model/consolidado.model'
import { Observable } from 'rxjs';

const resourceUrl  ='http://localhost:8080/'

@Injectable({
  providedIn: 'root'
})
export class ConsolidadoService {
  

  constructor( private http : HttpClient) { }

  findByPais(pais:string) : Observable<HttpResponse<Consolidado[]>>{
    return this.http.get<Consolidado[]>(`${resourceUrl}pais/${pais}`,{observe:'response'})
  }

  findByEstado(estado:string): Observable<HttpResponse<Consolidado[]>>{
    return this.http.get<Consolidado[]>(`${resourceUrl}estado/${estado}`,{observe:'response'})
  }

  findByCity(ciudad:string): Observable<HttpResponse<Consolidado[]>>{
    return this.http.get<Consolidado[]>(`${resourceUrl}ciudad/${ciudad}`,{observe:'response'})
  }
}
