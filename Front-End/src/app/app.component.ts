import { Component } from '@angular/core';
import { Consolidado } from './model/consolidado.model';
import { ConsolidadoService } from './services/consolidado.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent  {
  firstFormGroup : any;
  respuesta : Consolidado[] = []
  respuestaAux : string = ""
  tipoDeConsulta : string = ""
  lugar : string = ""
  verTabla=false
  
  
  constructor(
    private consolidadoService : ConsolidadoService,
  ){  }


  enviar():void{
    switch (this.tipoDeConsulta) {
      case 'ciudad':
        this.consolidadoService.findByCity(this.lugar).subscribe( 
          success => {
            if (success.body?.length === 0){
              this.respuestaAux="los parametros de su búsqueda no coinciden con ninguna ciudad"
            } else {
              this.respuesta=success.body!
              console.log(this.respuesta)
              this.verTabla= true
            }
           
          },
          error => {
            console.log(error)
          })
        break;
      case 'pais':
        this.consolidadoService.findByPais(this.lugar).subscribe( 
          success => {
            if (success.body?.length === 0){
              this.respuestaAux="los parametros de su búsqueda no coinciden con ningun país"
            } else {
              this.respuesta=success.body!
              console.log(this.respuesta)
              this.verTabla= true
            }
          },
          error => {
            console.log(error)
          })
        
          break;
      case 'estado':
        this.consolidadoService.findByEstado(this.lugar).subscribe( 
          success => {
            if (success.body?.length === 0){
              this.respuestaAux="los parametros de su búsqueda no coinciden con ningun estado"
            } else {
              this.respuesta=success.body!
              console.log(this.respuesta)
              this.verTabla= true
            }
          },
          error => {
            console.log(error)
          })
          break;
      default:
        this.respuestaAux="Se ha detectado que los parametros de tu busqueda no son claros, intenta con cambiar en los parametros por ciudad, estado, pais"

        break;
    }
  }
}